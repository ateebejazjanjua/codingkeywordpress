<?php
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\SMTP;
    use PHPMailer\PHPMailer\Exception;
    
    define ('SITE_ROOT', realpath(dirname(__DIR__)));
    require_once "../../env.php";
    require_once "../../vendor/autoload.php";
    define ('MAIL_RECEIVE', "muhammad.ali.5@tc-bpo.com");

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $name = strip_tags(trim($_POST["con_name"]));
        $name = str_replace(array("\r","\n"),array(" "," "),$name);

        $subject = strip_tags(trim($_POST["con_subject"]));
        $subject = str_replace(array("\r","\n"),array(" "," "),$subject);

        $email = filter_var(trim($_POST["con_email"]), FILTER_SANITIZE_EMAIL);

        $message = trim(nl2br(htmlspecialchars($_POST["con_message"])));
 
        if ( empty($name) OR empty($subject) OR empty($message) OR !filter_var($email, FILTER_VALIDATE_EMAIL)) {
            http_response_code(400);
            header('Location: /careers.php');
            exit();
            echo json_encode(
                array(
                    "type" => "error",
                    "status_code" => http_response_code(400),
                    "message" => "There was a problem with your submission, please try again."
                )
            );
            exit;
        }

        $status = upload_file('resume');

        if ($status) {
            $mail = new PHPMailer(true);
            $mail->SMTPDebug = MAIL_SMTPDEBUG;
            $mail->isSMTP();
            $mail->Host = MAIL_HOST;
            $mail->SMTPAuth = MAIL_SMTPAUTH;
            $mail->Username = MAIL_USERNAME;
            $mail->Password = MAIL_PASSWORD;
            $mail->SMTPSecure = MAIL_SMTPSECURE;
            $mail->Port = MAIL_PORT;

            $mail->From = MAIL_USERNAME;
            $mail->FromName = APP_NAME;

            $mail->addAddress(MAIL_RECEIVE, APP_NAME);
            
            $mail->isHTML(true);
            $mail->Subject = $subject;
            $mail->AddAttachment(SITE_ROOT.'/../uploads/'.$_FILES['resume']['name']);

            $email_content = "Email: $email\n";
            $email_content .= "<br />";
            $email_content .= "Name: $name\n";
            $email_content .= "<br />";
            $email_content .= "<p>$message</p>";

            $mail->Body = $email_content;

            
            try {
                $mail->send();
                http_response_code(200);
                header('Location: /careers.php');
                exit();
                echo json_encode(
                    array(
                        "type" => "success",
                        "status_code" => http_response_code(200),
                        "message" => "Message has been sent successfully",
                    )
                );
            } catch (Exception $e) {
                http_response_code(500);
                header('Location: /careers.php');
                exit();
                echo json_encode(
                    array(
                        "type" => "error",
                        "status_code" => http_response_code(500),
                        "message" => $mail->ErrorInfo,
                    )
                );
            }
        } else {
            http_response_code(303);
            header('Location: /careers.php');
            exit();
            echo json_encode(
                array(
                    "type" => "error",
                    "status_code" => http_response_code(303),
                    "message" => "An error occurred during the file upload!",
                    "FILES" => $_FILES,
                    "SITE_ROOT" => SITE_ROOT,
                )
            );
        }
    }else{
        http_response_code(403);
        header('Location: /careers.php');
        exit();
        echo json_encode(
            array(
                "type" => "error",
                "status_code" => http_response_code(403),
                "message" => "Please complete the form and try again.",
            )
        );
    }

    function upload_file($file_input, $upload_dir = SITE_ROOT.'/../uploads/'){
        if (!isset($_FILES[$file_input])) {
            return false;
        }

        // return false if error occurred
        $error = $_FILES[$file_input]['error'];

        if ($error !== UPLOAD_ERR_OK) {
            return false;
        }

        // move the uploaded file to the upload_dir
        $new_file = $upload_dir . $_FILES[$file_input]['name'];

        return move_uploaded_file(
            $_FILES[$file_input]['tmp_name'],
            $new_file
        );
    }
?>