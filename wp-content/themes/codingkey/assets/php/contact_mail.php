<?php
    use PHPMailer\PHPMailer\PHPMailer;
    use PHPMailer\PHPMailer\SMTP;
    use PHPMailer\PHPMailer\Exception;

    require_once "../../env.php";
    require_once "../../vendor/autoload.php";

    if ($_SERVER["REQUEST_METHOD"] == "POST") {
        $name = strip_tags(trim($_POST["con_name"]));
        $name = str_replace(array("\r","\n"),array(" "," "),$name);

        $phoneNumber = strip_tags(trim($_POST["con_phoneNumber"]));
        $phoneNumber = str_replace(array("\r","\n"),array(" "," "),$subject);

        $subject = strip_tags(trim($_POST["con_subject"]));
        $subject = str_replace(array("\r","\n"),array(" "," "),$subject);
        
        $email = filter_var(trim($_POST["con_email"]), FILTER_SANITIZE_EMAIL);
        $message = trim(nl2br(htmlspecialchars($_POST["con_message"])));
 
        if ( empty($name) OR empty($subject) OR empty($message) OR !filter_var($email, FILTER_VALIDATE_EMAIL)) {
            echo json_encode(
                array(
                    "type" => "error",
                    "status_code" => http_response_code(400),
                    "message" => "There was a problem with your submission, please try again."
                )
            );
            // echo "There was a problem with your submission, please try again.";
            exit;
        }

        $mail = new PHPMailer(true);
        $mail->SMTPDebug = MAIL_SMTPDEBUG;
        $mail->isSMTP();
        $mail->Host = MAIL_HOST;
        $mail->SMTPAuth = MAIL_SMTPAUTH;
        $mail->Username = MAIL_USERNAME;
        $mail->Password = MAIL_PASSWORD;
        $mail->SMTPSecure = MAIL_SMTPSECURE;
        $mail->Port = MAIL_PORT;

        $mail->From = MAIL_USERNAME;
        $mail->FromName = APP_NAME;

        $mail->addAddress(MAIL_RECEIVE, APP_NAME);
        
        $mail->isHTML(true);
        $mail->Subject = $subject;

        $email_content = "Email: $email\n";
        $email_content .= "<br />";
        $email_content .= "Name: $name\n";
        $email_content .= "<br />";
        $email_content .= "Phone Number: $phoneNumber\n";
        $email_content .= "<br />";
        $email_content .= "<p>$message</p>";

        $mail->Body = $email_content;

        try {
            $mail->send();
            echo json_encode(
                array(
                    "type" => "success",
                    "status_code" => http_response_code(200),
                    "message" => "Message has been sent successfully",
                )
            );
            // echo "Message has been sent successfully";
        } catch (Exception $e) {
            echo json_encode(
                array(
                    "type" => "error",
                    "status_code" => http_response_code(500),
                    "message" => $mail->ErrorInfo,
                )
            );
            // echo "Mailer Error: " . $mail->ErrorInfo;
        }
    }else{
        echo json_encode(
            array(
                "type" => "error",
                "status_code" => http_response_code(403),
                "message" => "Please complete the form and try again.",
            )
        );
        // echo "Please complete the form and try again.";
    }
?>