<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package codingkey
 */

?>
<?php if($post->ID != 17){ ?>
 <div class="contact-us-area infotechno-contact-us-bg section-space--pt_40">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-5">
                        <div class="image">
                            <img class="img-fluid" src="<?php bloginfo('stylesheet_directory');?>/assets/images/banners/home-cybersecurity-contact-bg-image.png"
                                alt="">
                        </div>
                    </div>

                    <div class="col-lg-4 ml-auto">
                        <div class="contact-info style-two text-left">

                            <div class="contact-list-item">
                                <a href="tel:9221111087423" class="single-contact-list">
                                    <div class="content-wrap">
                                        <div class="content">
                                            <div class="icon">
                                                <span class="fal fa-phone"></span>
                                            </div>
                                            <div class="main-content">
                                                <h6 class="heading">Call for advice now!</h6>
                                                <div class="text">+9221111087423</div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                                <a href="mailto:info@codingkey.com" class="single-contact-list">
                                    <div class="content-wrap">
                                        <div class="content">
                                            <div class="icon">
                                                <span class="fal fa-envelope"></span>
                                            </div>
                                            <div class="main-content">
                                                <h6 class="heading">Say hello</h6>
                                                <div class="text">info@codingKey.com</div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <?php } ?>
<div class="footer-area-wrapper black-bg">

    <div class="footer-area section-space--ptb_80">
        <div class="container">
            <div class="row footer-widget-wrapper border-bottom">
                <div class="col-lg-4 col-md-6 col-sm-6 footer-widget">
                    
                   <?php dynamic_sidebar('footer-1'); ?>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 footer-widget">
                     
                    <?php dynamic_sidebar('footer-2'); ?>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 footer-widget">
                    <?php dynamic_sidebar('footer-3'); ?>
                </div>


            </div>
        </div>
    </div>
    <div class="footer-copyright-area section-space--pb_30 small-mt__30">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-12 text-center">
                    <div class="footer-widget__logo mb-30">
                        <a href="/" class="d-table margin-auto"><img
                                    src="<?php bloginfo('stylesheet_directory')?>/assets/images/logo/logo-white.svg" alt="A Tribe Consulting Company"
                                    style="width: 200px;" class="img-fluid"> </a>
                        <a href="https://tc-bpo.com/" target="_blank" class="d-table margin-auto mb-3 tc-bpo">A Tribe
                            Consulting Company </a>
                    </div>
                    <span class="copyright-text text-white">&copy; <?= date('Y') ?> Codingkey.com All Rights Reserved.</span>
                </div>
            </div>
        </div>
    </div>
</div>

</div>

<!--====================  scroll top ====================-->
<a href="#" class="scroll-top" id="scroll-top">
    <i class="arrow-top fal fa-long-arrow-up"></i>
    <i class="arrow-bottom fal fa-long-arrow-up"></i>
</a>
<!--====================  End of scroll top  ====================-->

<!--====================  mobile menu overlay ====================-->
<div class="mobile-menu-overlay" id="mobile-menu-overlay">
    <div class="mobile-menu-overlay__inner">
        <div class="mobile-menu-overlay__header">
            <div class="container-fluid">
                <div class="row align-items-center">
                    <div class="col-md-6 col-8">
                        <!-- logo -->
                        <div class="logo">
                            <a href="/">
                                <img src="<?php bloginfo('stylesheet_directory')?>/assets/images/logo/logo.svg" class="img-fluid" alt="<?= APP_NAME ?>">
                            </a>
                        </div>
                    </div>
                    <div class="col-md-6 col-4">
                        <!-- mobile menu content -->
                        <div class="mobile-menu-content text-right">
                            <span class="mobile-navigation-close-icon" id="mobile-menu-close-trigger"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="mobile-menu-overlay__body">
            <nav class="offcanvas-navigation">
                <ul>
                    <li>
                        <a href="/"><span>Home</span></a>

                    </li>
                    <li>
                        <a href="/about-us"><span>About us</span></a>

                    </li>
                    <li class="has-children">
                        <a href="/services"><span>Services</span></a>

                        <ul class="sub-menu">
                            <li><a href="javascript:"><span>Mobile Development</span></a></li>
                            <li><a href="javascript:"><span>Database</span></a></li>
                            <li><a href="javascript:l"><span>Software Development</span></a></li>
                            <li><a href="javascript:"><span> Quality Assurance</span></a></li>
                            <li><a href="javascript:"><span>CMS (Content Managment System)</span></a></li>
                            <li><a href="javascript:"><span>UI/UX Design</span></a></li>
                            <li><a href="javascript:"><span>Marketing</span></a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="/team"><span>Leadership Team</span></a>

                    </li>
                    <li>
                        <a href="/careers"><span>Career</span></a>
                    </li>
                    <li>
                        <a href="/contact-us"><span>Contact Us</span></a>
                    </li>

                </ul>
            </nav>
        </div>
    </div>
</div>
<!--====================  End of mobile menu overlay  ====================-->


<!--====================  search overlay ====================-->
<div class="search-overlay" id="search-overlay">

    <div class="search-overlay__header">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-md-6 ml-auto col-4">
                    <!-- search content -->
                    <div class="search-content text-right">
                        <span class="mobile-navigation-close-icon" id="search-close-trigger"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="search-overlay__inner">
        <div class="search-overlay__body">
            <div class="search-overlay__form">
                <form action="#">
                    <input type="text" placeholder="Search">
                </form>
            </div>
        </div>
    </div>
</div>
<!--====================  End of search overlay  ====================-->

<!-- JS
============================================ -->
<!-- Modernizer JS -->
<script src="<?php bloginfo('stylesheet_directory')?>/assets/js/vendor/modernizr-2.8.3.min.js"></script>

<!-- jQuery JS -->
<script src="<?php bloginfo('stylesheet_directory')?>/assets/js/vendor/jquery-3.5.1.min.js"></script>
<script src="<?php bloginfo('stylesheet_directory')?>/assets/js/vendor/jquery-migrate-3.3.0.min.js"></script>

<!-- Bootstrap JS -->
<script src="<?php bloginfo('stylesheet_directory')?>/assets/js/vendor/bootstrap.min.js"></script>

<!-- wow JS -->
<script src="<?php bloginfo('stylesheet_directory')?>/assets/js/plugins/wow.min.js"></script>

<!-- Swiper Slider JS -->
<script src="<?php bloginfo('stylesheet_directory')?>/assets/js/plugins/swiper.min.js"></script>

<!-- Light gallery JS -->
<script src="<?php bloginfo('stylesheet_directory')?>/assets/js/plugins/lightgallery.min.js"></script>

<!-- Waypoints JS -->
<script src="<?php bloginfo('stylesheet_directory')?>/assets/js/plugins/waypoints.min.js"></script>

<!-- Counter down JS -->
<script src="<?php bloginfo('stylesheet_directory')?>/assets/js/plugins/countdown.min.js"></script>

<!-- Isotope JS -->
<script src="<?php bloginfo('stylesheet_directory')?>/assets/js/plugins/isotope.min.js"></script>

<!-- Masonry JS -->
<script src="<?php bloginfo('stylesheet_directory')?>/assets/js/plugins/masonry.min.js"></script>

<!-- ImagesLoaded JS -->
<script src="<?php bloginfo('stylesheet_directory')?>/assets/js/plugins/images-loaded.min.js"></script>

<!-- Wavify JS -->
<script src="<?php bloginfo('stylesheet_directory')?>/assets/js/plugins/wavify.js"></script>

<!-- jQuery Wavify JS -->
<script src="<?php bloginfo('stylesheet_directory')?>/assets/js/plugins/jquery.wavify.js"></script>

<!-- circle progress JS -->
<script src="<?php bloginfo('stylesheet_directory')?>/assets/js/plugins/circle-progress.min.js"></script>

<!-- counterup JS -->
<script src="<?php bloginfo('stylesheet_directory')?>/assets/js/plugins/counterup.min.js"></script>

<!-- animation text JS -->
<script src="<?php bloginfo('stylesheet_directory')?>/assets/js/plugins/animation-text.min.js"></script>

<!-- Vivus JS -->
<script src="<?php bloginfo('stylesheet_directory')?>/assets/js/plugins/vivus.min.js"></script>

<!-- Some plugins JS -->
<script src="<?php bloginfo('stylesheet_directory')?>/assets/js/plugins/some-plugins.js"></script>


<!-- Plugins JS (Please remove the comment from below plugins.min.js for better website load performance and remove plugin js files from avobe) -->

<!--
<script src="assets/js/plugins/plugins.min.js"></script>
-->

<!-- Main JS -->
<script src="<?php bloginfo('stylesheet_directory')?>/assets/js/main.js"></script>

<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-T9HHZC3"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) --


<?php wp_footer(); ?>

</body>
</html>
