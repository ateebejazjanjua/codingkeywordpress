<?php
/**
 * The template for displaying all pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site may use a
 * different template.
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package codingkey
 */

get_header();
?>

<?php $className =  get_field('background_image_class');?>
 <div class="about-banner-wrap banner-space <?php echo $className;?>">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 ml-auto mr-auto">
                    <div class="about-banner-content text-center">
                        <h1 class="mb-15 text-white"><?php the_title(); ?></h1>
                        <h5 class="font-weight--normal text-white">
                            <?= get_field('banner_short_description');?>
                        </h5>
                    </div>
                </div>
            </div>
        </div>
    </div>

<div id="main-wrapper" class="mt-40">

    <div class="site-wrapper-reveal">
        <div class="container">
		<?php
		while ( have_posts() ) :
			the_post();

		 the_content();

		 

		endwhile; // End of the loop.
		?>
    </div>
    </div>
</div>

<?php
 
get_footer();
