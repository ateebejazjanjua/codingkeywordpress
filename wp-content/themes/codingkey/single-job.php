<?php

get_header();
?>


    <!-- breadcrumb-area start -->
    <div class="breadcrumb-area">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <div class="breadcrumb_box text-center">
                        <h2 class="breadcrumb-title"><?= $dataCount > 0 ? $data['position'] : ""; ?></h2>
                        <!-- breadcrumb-list start -->
                        <ul class="breadcrumb-list">
                            <li class="breadcrumb-item"><a href="<?= SITE_URL ?>">Home</a></li>
                            <li class="breadcrumb-item active">Careers</li>
                        </ul>
                        <!-- breadcrumb-list end -->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- breadcrumb-area end -->


    <div id="main-wrapper">
    <div class="site-wrapper-reveal">
        <!--====================  Conact us Section Start ====================-->
        <div class="contact-us-section-wrappaer section-space--pt_100 section-space--pb_70">
            <div class="container">
                <div class="row">


                    <div class="col-lg-12 col-lg-12">
                        <div class="job-details">
<?php
                // Start the Loop.
                while ( have_posts() ) : the_post(); ?>
 

              
                            <h3><?php the_title(); ?></h3>
                            <span><strong>Location: </strong> <?= get_field('job_location'); ?></span>
                            <span><strong>Timings: </strong>   <?= get_field('job_timings');?></span>

                                <?php the_content();?>
 

                        </div>
                        
                        <?php   endwhile;
            ?>

                    </div>

                    <div class="col-lg-12 col-lg-12">
                        <div class="contact-form-wrap">
                             
                                 <?php echo do_shortcode('[contact-form-7 id="73" title="Careers"]'); ?>
                                
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--====================  Conact us Section End  ====================-->

        <!--====================  Conact us info Start ====================-->
        <div class="contact-us-info-wrappaer section-space--pb_100">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6 col-md-6">
                        <div class="conact-info-wrap mt-30">
                            <h5 class="heading mb-20">Karachi</h5>
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item"
                                        src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3318.0333347123387!2d73.08641116520671!3d33.73395413069367!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38dfc1cc98518753%3A0xaf7f63890ee82e32!2sTribe%20Consulting%20Islamabad!5e0!3m2!1sen!2s!4v1630080789202!5m2!1sen!2s"
                                        width="600" height="450" style="border:0;" allowfullscreen=""
                                        loading="lazy"></iframe>
                            </div>
                            <ul class="conact-info__list mt-4">
                                <li><a href="mailto:<?= INFO_EMAIL ?>" class="hover-style-link text-color-primary"><i
                                                class="far fa-envelope mr-2 fa-fw" style="color: #43B7C5;"></i>
                                        <?= INFO_EMAIL ?></a></li>
                                <li><a href="tel:<?= CONTACT_KARACHI ?>"
                                       class="hover-style-link text-black font-weight--bold"><i
                                                class="far fa-phone mr-2 fa-fw" style="color: #43B7C5;"></i>
                                        <?= CONTACT_KARACHI ?></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6">
                        <div class="conact-info-wrap mt-30">
                            <h5 class="heading mb-20">Islamabad</h5>
                            <div class="embed-responsive embed-responsive-16by9">
                                <iframe class="embed-responsive-item"
                                        src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3318.0333347123387!2d73.08641116520671!3d33.73395413069367!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x38dfc1cc98518753%3A0xaf7f63890ee82e32!2sTribe%20Consulting%20Islamabad!5e0!3m2!1sen!2s!4v1630080789202!5m2!1sen!2s"
                                        width="600" height="450" style="border:0;" allowfullscreen=""
                                        loading="lazy"></iframe>
                            </div>
                            <ul class="conact-info__list mt-4">
                                <li><a href="mailto:<?= INFO_EMAIL ?>" class="hover-style-link text-color-primary"><i
                                                class="far fa-envelope mr-2 fa-fw" style="color: #43B7C5;"></i>
                                        <?= INFO_EMAIL ?></a></li>
                                <li><a href="tel:<?= CONTACT_ISLAMABAD ?>"
                                       class="hover-style-link text-black font-weight--bold"><i
                                                class="far fa-phone mr-2 fa-fw" style="color: #43B7C5;"></i>
                                        <?= CONTACT_ISLAMABAD ?></a></li>
                            </ul>
                        </div>
                    </div>


                </div>
            </div>
        </div>
        <!--====================  Conact us info End  ====================-->


        <!--========== Call to Action Area Start ============-->
        <div class="cta-image-area_one section-space--ptb_80 cta-bg-image_two">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-xl-8 col-lg-7">
                        <div class="cta-content md-text-center">
                            <h3 class="heading">We run all kinds of IT services that vow your <span
                                        class="text-color-primary"> success</span></h3>
                        </div>
                    </div>
                    <div class="col-xl-4 col-lg-5">
                        <div class="cta-button-group--two text-center">
                            <a href="#" class="btn btn--white btn-one"><span class="btn-icon mr-2"><i
                                            class="far fa-comment-alt-dots"></i></span> Let's talk</a>
                            <a href="#" class="btn btn--secondary btn-two "><span class="btn-icon mr-2"><i
                                            class="far fa-info-circle"></i></span> Get info</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--========== Call to Action Area End ============-->


    </div>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

<script>  
// var job_title = getUrlVars()["jobtitle"];
//  var jobtitle = job_title.replace(/%20/g, " ");
<?php
global $post;
echo $id = get_the_ID();
$jobTitle = get_the_title($id); ?>

var jobtitle = '<?php echo $jobTitle ?>';
//alert(jobtitle);
 $('#job-position').val(jobtitle);
 $('#job-position').attr('readonly', true);

 
console.log(jobtitle);
 
</script> 



<?php
get_footer();?>


