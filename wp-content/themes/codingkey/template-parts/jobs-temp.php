<?php 
/*
Template Name: Jobs Listing

*/

get_header();
?>


    <div class="about-banner-wrap banner-space about-us-bg career-bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 ml-auto mr-auto">
                    <div class="about-banner-content text-center">
                        <h1 class="mb-15 text-white">Careers</h1>
                        <h5 class="font-weight--normal text-white">
                            Become a part of our big family to inspire and get inspired bys <span
                                    class="text-color-primary"> professional experts</span>
                        </h5>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div id="main-wrapper">
    <div class="site-wrapper-reveal">
        <!--======== careers-experts Start ==========-->
        <div class="careers-experts-wrapper section-space--pt_100">
            <div class="container">
                <div class="row">
                    <div class="col-lg-9 ml-auto mr-auto">
                        <!-- section-title-wrap Start -->
                        <div class="section-title-wrap text-center section-space--mb_30">
                            <h3 class="heading">Opened Positions at <span
                                        class="text-color-primary"> CodingKey </span></h3>
                        </div>
                        <!-- section-title-wrap Start -->
                    </div>
                </div>


                <div class="row">
                    <div class="col-lg-12">
                        <div class="ht-simple-job-listing move-up animate">
                            <div clas="list">
                                 <?php
                                 
                                 $args = array(  
                                'post_type' => 'job',
                                'post_status' => 'publish',
                                'posts_per_page' => -1, 
                                'orderby' => 'menu_order', 
                            );
                        
                            $loop = new WP_Query( $args ); 
                                
                            while ( $loop->have_posts() ) : $loop->the_post(); 
                               ?>
                                        <div class="item">
                                            <div class="row">
                                                <div class="col-md-3">
                                                    <div class="job-info">
                                                        <h5 class="job-name"><?php  the_title(); ?></h5>
                                                        <span class="job-time"><?= get_field('job_timings', $post->ID); ?></span>
                                                    </div>
                                                </div>

                                                <div class="col-md-6">
                                                    <div class="job-description mb-4">
                                                        <?php  the_excerpt();  ?>
                                                    </div>
                                                    <span class="mr-3 job-tags"><strong>Posted on:</strong> <?= get_field('job_timings', $post->ID); ?>  </span>
                                                    <span class="mr-3"><strong>Status:</strong>  <?= get_field('job_status', $post->ID); ?></span>
                                                    <span class="mr-3"><strong>Location:</strong> <?= get_field('job_location', $post->ID); ?> </span>
                                                </div>

                                                <div class="col-md-3">
                                                    <div class="job-button text-md-center">
                                                        <a class="ht-btn ht-btn-md ht-btn--solid"
                                                           href="<?php the_permalink();?>">
                                                            <span class="button-text">APPLY NOW</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    endwhile;
                        
                            wp_reset_postdata(); 
                                ?>
                                

                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>

        <div class="gallery-section section-space--ptb_100">
            <div class="container">

                <!-- gallery-wrapper Start -->
                <div class="gallery-wrapper section-space--pt_80 ">
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- gallery-warp Start -->
                            <div class="gallery-warp popup-images ">
                                <!-- single gallery Start -->
                                <a class="single-gallery gallery-grid wow move-up animated"
                                   href="<?php bloginfo('stylesheet_directory'); ?>/assets/images/activities/codingkey-activities12.jpeg"
                                   style="visibility: visible;">
                                    <div class="single-gallery__item">
                                        <div class="single-gallery__thum bg-item-images bg-img"
                                             data-bg="<?php bloginfo('stylesheet_directory'); ?>/assets/images/activities/codingkey-activities12.jpeg"
                                             style="background-image: url(&quot;<?php bloginfo('stylesheet_directory'); ?>/assets/images/activities/codingkey-activities12.jpeg&quot;);">

                                        </div>
                                        <div class="ht-overlay"></div>
                                        <div class="overlay-icon">
                                            <i class="far fa-search"></i>
                                        </div>
                                    </div>
                                </a>
                                <!-- single gallery End -->
                                <!-- single gallery Start -->
                                <a class="single-gallery gallery-grid wow move-up animated"
                                   href="<?php bloginfo('stylesheet_directory'); ?>/assets/images/activities/codingkey-activities3.jpeg"
                                   style="visibility: visible;">
                                    <div class="single-gallery__item">
                                        <div class="single-gallery__thum bg-item-images bg-img"
                                             data-bg="<?php bloginfo('stylesheet_directory'); ?>/assets/images/activities/codingkey-activities3.jpeg"
                                             style="background-image: url(&quot;<?php bloginfo('stylesheet_directory'); ?>/assets/images/activities/codingkey-activities3.jpeg&quot;);"></div>
                                        <div class="ht-overlay"></div>
                                        <div class="overlay-icon">
                                            <i class="far fa-search"></i>
                                        </div>
                                    </div>
                                </a>
                                <!-- single gallery End -->
                                <!-- single gallery Start -->
                                <a class="single-gallery gallery-grid wow move-up animated"
                                   href="<?php bloginfo('stylesheet_directory'); ?>/assets/images/activities/codingkey-activities19.jpeg"
                                   style="visibility: visible;">
                                    <div class="single-gallery__item">
                                        <div class="single-gallery__thum bg-item-images bg-img"
                                             data-bg="<?php bloginfo('stylesheet_directory'); ?>/assets/images/activities/codingkey-activities19.jpeg"
                                             style="background-image: url(&quot;<?php bloginfo('stylesheet_directory'); ?>/assets/images/activities/codingkey-activities19.jpeg&quot;);"></div>
                                        <div class="ht-overlay"></div>
                                        <div class="overlay-icon">
                                            <i class="far fa-search"></i>
                                        </div>
                                    </div>
                                </a>
                                <!-- single gallery End -->
                                <!-- single gallery Start -->
                                <a class="single-gallery gallery-grid wow move-up animated"
                                   href="<?php bloginfo('stylesheet_directory'); ?>/assets/images/activities/codingkey-activities4.jpeg"
                                   style="visibility: visible;">
                                    <div class="single-gallery__item">
                                        <div class="single-gallery__thum bg-item-images bg-img"
                                             data-bg="<?php bloginfo('stylesheet_directory'); ?>/assets/images/activities/codingkey-activities4.jpeg"
                                             style="background-image: url(&quot;<?php bloginfo('stylesheet_directory'); ?>/assets/images/activities/codingkey-activities4.jpeg&quot;);"></div>
                                        <div class="ht-overlay"></div>
                                        <div class="overlay-icon">
                                            <i class="far fa-search"></i>
                                        </div>
                                    </div>
                                </a>

                                <!-- single gallery End -->
                                <!-- single gallery Start -->
                                <a class="single-gallery gallery-grid wow move-up animated"
                                   href="<?php bloginfo('stylesheet_directory'); ?>/assets/images/activities/codingkey-activities13.jpeg"
                                   style="visibility: visible;">
                                    <div class="single-gallery__item">
                                        <div class="single-gallery__thum bg-item-images bg-img"
                                             data-bg="<?php bloginfo('stylesheet_directory'); ?>/assets/images/activities/codingkey-activities13.jpeg"
                                             style="background-image: url(&quot;<?php bloginfo('stylesheet_directory'); ?>/assets/images/activities/codingkey-activities13.jpeg&quot;);"></div>
                                        <div class="ht-overlay"></div>
                                        <div class="overlay-icon">
                                            <i class="far fa-search"></i>
                                        </div>
                                    </div>
                                </a>

                                <a class="single-gallery gallery-grid wow move-up animated"
                                   href="<?php bloginfo('stylesheet_directory'); ?>/assets/images/activities/codingkey-activities17.jpeg"
                                   style="visibility: visible;">
                                    <div class="single-gallery__item">
                                        <div class="single-gallery__thum bg-item-images bg-img"
                                             data-bg="<?php bloginfo('stylesheet_directory'); ?>/assets/images/activities/codingkey-activities17.jpeg"
                                             style="background-image: url(&quot;<?php bloginfo('stylesheet_directory'); ?>/assets/images/activities/codingkey-activities17.jpeg&quot;);">

                                        </div>
                                        <div class="ht-overlay"></div>
                                        <div class="overlay-icon">
                                            <i class="far fa-search"></i>
                                        </div>
                                    </div>
                                </a>


                            </div>
                            <!-- gallery-warp End -->
                        </div>
                    </div>
                </div>
                <!-- gallery-wrapper End -->

            </div>
        </div>

        <div class="contact-us-area infotechno-contact-us-bg section-space--pt_40">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-5">
                        <div class="image">
                            <img class="img-fluid"
                                 src="<?php bloginfo('stylesheet_directory'); ?>/assets/images/banners/home-cybersecurity-contact-bg-image.png" alt="">
                        </div>
                    </div>

                    <div class="col-lg-4 ml-auto">
                        <div class="contact-info style-two text-left">

                            <div class="contact-list-item">
                                <a href="tel:<?= CONTACT_KARACHI ?>" class="single-contact-list">
                                    <div class="content-wrap">
                                        <div class="content">
                                            <div class="icon">
                                                <span class="fal fa-phone"></span>
                                            </div>
                                            <div class="main-content">
                                                <h6 class="heading">Call for advice now!</h6>
                                                <div class="text"><?= CONTACT_KARACHI ?></div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                                <a href="mailto:<?= INFO_EMAIL ?>" class="single-contact-list">
                                    <div class="content-wrap">
                                        <div class="content">
                                            <div class="icon">
                                                <span class="fal fa-envelope"></span>
                                            </div>
                                            <div class="main-content">
                                                <h6 class="heading">Say hello</h6>
                                                <div class="text"><?= INFO_EMAIL ?></div>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>




<?php 
get_footer();
?>