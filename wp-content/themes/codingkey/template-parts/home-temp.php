<?php
/*
Template Name: Home Template
*/

get_header();
?>

  <div id="main-wrapper">
    <div class="site-wrapper-reveal">
        <div class="modern-it-company-top-area">

            <!--============ Modern It Company Hero Start ============-->
            <div class="modern-it-company-hero">

                <div class="container">
                    <div class="row align-items-center">
                        <div class="modern-it-company-hero-image">
                            <img src="<?php bloginfo('stylesheet_directory')?>/assets/images/hero/home-reputable-success.png" class="img-fluid" alt="">
                        </div>
                        <div class="col-lg-9 col-md-9">
                            <div class="modern-it-company-hero-text  wow move-up section-space--pt_150">
                                <h1 class="heading">Leave Your Digital Worries to
                                    <mark class="text-color-primary"> CodingKey</mark>
                                </h1>
                                <h6 class="sort-dec">Our technology experts can take care of your problems, you can
                                    simply outsource and relax</h6>


                                <div class="book-box">
                                    <div class="image">
                                        <img src="<?php bloginfo('stylesheet_directory')?>/assets/images/icons/green-curve-arrow.png" class="img-fluid"
                                             alt="">
                                    </div>
                                    <h5 class="book-text font-weight--bold mb-15">Connect with our Consultants!</h5>
                                </div>

                                <?php echo do_shortcode('[contact-form-7 id="225" title="Connect with our Consultants!"]'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--============ Modern It Company Hero End ============-->

            <!--====================  brand logo slider area ====================-->
            <div class="brand-logo-slider-area section-space--ptb_80">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="section-title section-space--mb_60">
                                <h5 class="heading-text text-center">

                                    Join the emerging success stories that are taking advantage of CodingKeys'
                                    services and take your business to the next level
                                </h5>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <!-- brand logo slider -->
                            <div class="brand-logo-slider__container-area">
                                <div class="swiper-container brand-logo-slider__container">
                                    <div class="swiper-wrapper brand-logo-slider__one">
                                        <div class="swiper-slide brand-logo brand-logo--slider">
                                            <a href="#">
                                                <div class="brand-logo__image">
                                                    <img src="<?php bloginfo('stylesheet_directory')?>/assets/images/brand/clientpoint.png" class="img-fluid"
                                                         alt="">
                                                </div>
                                                <div class="brand-logo__image-hover">
                                                    <img src="<?php bloginfo('stylesheet_directory')?>/assets/images/brand/clientpoint-hover.png"
                                                         class="img-fluid" alt="">
                                                </div>
                                            </a>
                                        </div>
                                        <div class="swiper-slide brand-logo brand-logo--slider">
                                            <a href="#">
                                                <div class="brand-logo__image">
                                                    <img src="<?php bloginfo('stylesheet_directory')?>/assets/images/brand/zoho.png" class="img-fluid"
                                                         alt="">
                                                </div>
                                                <div class="brand-logo__image-hover">
                                                    <img src="<?php bloginfo('stylesheet_directory')?>/assets/images/brand/zoho-hover.png" class="img-fluid"
                                                         alt="">
                                                </div>
                                            </a>
                                        </div>
                                        <div class="swiper-slide brand-logo brand-logo--slider">
                                            <a href="#">
                                                <div class="brand-logo__image">
                                                    <img src="<?php bloginfo('stylesheet_directory')?>/assets/images/brand/medicalwinglocums.png"
                                                         class="img-fluid" alt="">
                                                </div>
                                                <div class="brand-logo__image-hover">
                                                    <img src="<?php bloginfo('stylesheet_directory')?>/assets/images/brand/medicalwinglocums-hover.png"
                                                         class="img-fluid" alt="">
                                                </div>
                                            </a>
                                        </div>
                                        <div class="swiper-slide brand-logo brand-logo--slider">
                                            <a href="#">
                                                <div class="brand-logo__image">
                                                    <img src="<?php bloginfo('stylesheet_directory')?>/assets/images/brand/bitrix24.png" class="img-fluid"
                                                         alt="">
                                                </div>
                                                <div class="brand-logo__image-hover">
                                                    <img src="<?php bloginfo('stylesheet_directory')?>/assets/images/brand/bitrix24-hover.png"
                                                         class="img-fluid" alt="">
                                                </div>
                                            </a>
                                        </div>
                                        <div class="swiper-slide brand-logo brand-logo--slider">
                                            <a href="#">
                                                <div class="brand-logo__image">
                                                    <img src="<?php bloginfo('stylesheet_directory')?>/assets/images/brand/ozonetel.png" class="img-fluid"
                                                         alt="">
                                                </div>
                                                <div class="brand-logo__image-hover">
                                                    <img src="<?php bloginfo('stylesheet_directory')?>/assets/images/brand/ozonetel-hover.png"
                                                         class="img-fluid" alt="">
                                                </div>
                                            </a>
                                        </div>
                                        <div class="swiper-slide brand-logo brand-logo--slider">
                                            <a href="#">
                                                <div class="brand-logo__image">
                                                    <img src="<?php bloginfo('stylesheet_directory')?>/assets/images/brand/five9.png" class="img-fluid"
                                                         alt="">
                                                </div>
                                                <div class="brand-logo__image-hover">
                                                    <img src="<?php bloginfo('stylesheet_directory')?>/assets/images/brand/five9-hover.png" class="img-fluid"
                                                         alt="">
                                                </div>
                                            </a>
                                        </div>
                                        <div class="swiper-slide brand-logo brand-logo--slider">
                                            <a href="#">
                                                <div class="brand-logo__image">
                                                    <img src="<?php bloginfo('stylesheet_directory')?>/assets/images/brand/gsuite.png" class="img-fluid"
                                                         alt="">
                                                </div>
                                                <div class="brand-logo__image-hover">
                                                    <img src="<?php bloginfo('stylesheet_directory')?>/assets/images/brand/gsuite-hover.png"
                                                         class="img-fluid" alt="">
                                                </div>
                                            </a>
                                        </div>
                                        <div class="swiper-slide brand-logo brand-logo--slider">
                                            <a href="#">
                                                <div class="brand-logo__image">
                                                    <img src="<?php bloginfo('stylesheet_directory')?>/assets/images/brand/twilio.png" class="img-fluid"
                                                         alt="">
                                                </div>
                                                <div class="brand-logo__image-hover">
                                                    <img src="<?php bloginfo('stylesheet_directory')?>/assets/images/brand/twilio-hover.png"
                                                         class="img-fluid" alt="">
                                                </div>
                                            </a>
                                        </div>
                                        <div class="swiper-slide brand-logo brand-logo--slider">
                                            <a href="#">
                                                <div class="brand-logo__image">
                                                    <img src="<?php bloginfo('stylesheet_directory')?>/assets/images/brand/teamwork.png" class="img-fluid"
                                                         alt="">
                                                </div>
                                                <div class="brand-logo__image-hover">
                                                    <img src="<?php bloginfo('stylesheet_directory')?>/assets/images/brand/teamwork-hover.png"
                                                         class="img-fluid" alt="">
                                                </div>
                                            </a>


                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!--====================  End of brand logo slider area  ====================-->

            <!-- ================  Clients Review Area Start ======================= -->
            <div class="modern-it-company-clients-review-area modern-it-company-testimonial-bg section-space--pb_120">
                <div class="container-fluid">
                    <div class="row align-items-center">
                        <div class="col-lg-6 col-md-6">
                            <div class="image text-center">
                                <img src="<?php bloginfo('stylesheet_directory')?>/assets/images/CodingKeys.svg" class="img-fluid" alt="">
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6">
                            <div class="modern-it-company-testimonial text-left">
                                <h3 class="mb-2">We help your ideas fly</h3>
                                <p class="text">With reliable and creative experts in a wide range of technical
                                    domains, businesses lean towards CodingKey to achieve their set goals with
                                    optimal results. We aim to find the solutions of the problems our clients face,
                                    and help them refine their ideas through strategic collaborations, resulting in
                                    fast paced product growth.</p>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- ================  Clients Review Area End ======================= -->

        </div>

        <!--=========== fun fact Wrapper Start ==========-->
        <div class="fun-fact-wrapper bg-gray section-space--pb_30 section-space--pt_60">
            <div class="container">

                <div class="row">
                    <div class="col-lg-12">
                        <!-- section-title-wrap Start -->
                        <div class="section-title-wrap text-center section-space--mb_60">
                            <h2 class="heading">Our Team's Achievements</h2>
                            <p class="text-center">
                                CodingKey is a team of senior industry professionals with proven delivery track
                                record and international exposure
                            </p>
                        </div>
                        <!-- section-title-wrap Start -->
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-4 col-sm-6">
                        <div class="fun-fact--four text-center">
                            <div class="icon">
                                <i class="fal fa-calendar"></i>
                            </div>
                            <div class="fun-fact-text">
                                <div class="fun-fact__count counter">8</div>
                                <span class="sub-counter">+ Years</span>
                                <h6 class="fun-fact__text">Average Experience</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="fun-fact--four text-center">
                            <div class="icon">
                                <i class="fal fa-thumbs-up"></i>
                            </div>
                            <div class="fun-fact-text">
                                <div class="fun-fact__count counter">500</div>
                                <span class="sub-counter">+ Projects</span>
                                <h6 class="fun-fact__text">Successful Deliveries</h6>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="fun-fact--four text-center">
                            <div class="icon">
                                <i class="fal fa-globe"></i>
                            </div>
                            <div class="fun-fact-text">
                                <div class="fun-fact__count counter">4</div>
                                <span class="sub-counter">Continents</span>
                                <h6 class="fun-fact__text">Customer Coverage</h6>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <!--=========== fun fact Wrapper End ==========-->


        <!--===========  Feature Images Wrapper Start =============-->
        <div class="feature-images-wrapper bg-gradient section-space--ptb_100">
            <div class="container">

                <div class="row">
                    <div class="col-lg-12">
                        <!-- section-title-wrap Start -->
                        <div class="section-title-wrap text-center section-space--mb_60">
                            <h2 class="heading">Our Core Services</h2>
                            <p class="text-center">
                                In a short time, CodingKey has achieved immense recognition across wide range of
                                service domains
                            </p>
                        </div>
                        <!-- section-title-wrap Start -->
                    </div>
                </div>


                <div class="image-l-r-box">
                    <div class="row">
                        <div class="col image-box-area">
                            <div class="row image-box-boder-box">

                                <div class="col-lg-4 col-md-4 image-box-boder">
                                    <!-- ht-box-icon Start -->
                                    <div class="ht-box-images style-09">
                                        <div class="image-box-wrap">
                                            <div class="box-image">
                                                <img src="<?php bloginfo('stylesheet_directory')?>/assets/images/services/software development.svg"
                                                     class="img-fluid" alt="">
                                            </div>
                                            <div class="content">
                                                <h6 class="heading">Cloud Solutions</h6>
                                                <div class="text">
                                                    We know data is the most important piece of your business. Our
                                                    cloud solutions are digitally engineered to combine the security
                                                    and scalability in an application that will run faster and
                                                    require less maintenance.
                                                </div>
                                                <div class="more-arrow-link">
                                                    <a href="#">
                                                        Learn more <i class="far fa-long-arrow-right"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- ht-box-icon End -->
                                </div>

                                <div class="col-lg-4 col-md-4 image-box-boder">
                                    <!-- ht-box-icon Start -->
                                    <div class="ht-box-images style-09">
                                        <div class="image-box-wrap">
                                            <div class="box-image">
                                                <img src="<?php bloginfo('stylesheet_directory')?>/assets/images/services/mobiledeveloppment.svg"
                                                     class="img-fluid" alt="">
                                            </div>
                                            <div class="content">
                                                <h6 class="heading">Mobile Apps Development </h6>
                                                <div class="text">We offer a full cycle of application design,
                                                    integration, management services and efficient features that are
                                                    user-friendly and increase productivity and sales. Whether it is
                                                    a consumer-oriented app or an enterprise-class solution, the
                                                    company leads through the process of creating smart mobile
                                                    solutions, from ideation and concept, to delivery, and to
                                                    ongoing support.
                                                </div>
                                                <div class="more-arrow-link">
                                                    <a href="#">
                                                        Learn more <i class="far fa-long-arrow-right"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- ht-box-icon End -->
                                </div>


                                <div class="col-lg-4 col-md-4 image-box-boder">
                                    <!-- ht-box-icon Start -->
                                    <div class="ht-box-images style-09">
                                        <div class="image-box-wrap">
                                            <div class="box-image">
                                                <img src="<?php bloginfo('stylesheet_directory')?>/assets/images/services/qualityassurance.svg"
                                                     class="img-fluid" alt="">
                                            </div>
                                            <div class="content">
                                                <h6 class="heading">Quality Assurance</h6>
                                                <div class="text">We offer a comprehensive set of software quality
                                                    assurance services which ensures concrete control over product’s
                                                    life cycle, monitor every development stage, and give accurate
                                                    product quality information. Our QA experts have been using the
                                                    latest practices and technologies to ensure efficient
                                                    performance and delivery of High-Quality Software within the
                                                    required timeframe.

                                                </div>
                                                <div class="more-arrow-link">
                                                    <a href="#">
                                                        Learn more <i class="far fa-long-arrow-right"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- ht-box-icon End -->
                                </div>


                                <div class="col-lg-4 col-md-4 image-box-boder">
                                    <!-- ht-box-icon Start -->
                                    <div class="ht-box-images style-09">
                                        <div class="image-box-wrap">
                                            <div class="box-image">
                                                <img src="<?php bloginfo('stylesheet_directory')?>/assets/images/services/qualityassurance.svg"
                                                     class="img-fluid" alt="">
                                            </div>
                                            <div class="content">
                                                <h6 class="heading">Testing & Automation</h6>
                                                <div class="text">We ensure fast delivery of software without losing
                                                    quality, as we employ a selected approach to automated QA where
                                                    our team has standardized skills using various tools and
                                                    technologies with minimizing risks. Our DevOps engineers and QA
                                                    testers work side by side to allow for seamless and continuous
                                                    agile delivery.

                                                </div>
                                                <div class="more-arrow-link">
                                                    <a href="#">
                                                        Learn more <i class="far fa-long-arrow-right"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- ht-box-icon End -->
                                </div>


                                <div class="col-lg-4 col-md-4 image-box-boder">
                                    <!-- ht-box-icon Start -->
                                    <div class="ht-box-images style-09">
                                        <div class="image-box-wrap">
                                            <div class="box-image">
                                                <img src="<?php bloginfo('stylesheet_directory')?>/assets/images/services/database.svg" class="img-fluid"
                                                     alt="">
                                            </div>
                                            <div class="content">
                                                <h6 class="heading">Data Sciene</h6>
                                                <div class="text">Our team has an abundance of expertise in creating
                                                    solutions for big data analysis, customer journey mapping,
                                                    designing and implementing data warehousing systems. Our insight
                                                    into business issues and opportunities allows our customers to
                                                    generate both the right questions and the right answers.
                                                </div>
                                                <div class="more-arrow-link">
                                                    <a href="#">
                                                        Learn more <i class="far fa-long-arrow-right"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- ht-box-icon End -->
                                </div>


                                <div class="col-lg-4 col-md-4 image-box-boder">
                                    <!-- ht-box-icon Start -->
                                    <div class="ht-box-images style-09">
                                        <div class="image-box-wrap">
                                            <div class="box-image">
                                                <img src="<?php bloginfo('stylesheet_directory')?>/assets/images/services/iot.svg" class="img-fluid" alt="">
                                            </div>
                                            <div class="content">
                                                <h6 class="heading">Internet of Things (IOT)</h6>
                                                <div class="text">Our IoT managed services are a set of end to end
                                                    services designed to connect all your operations to a single
                                                    network using sensors, software, network connectivity tools, and
                                                    necessary electronics that not only let your business to enhance
                                                    productivity but helps in providing you with the best IoT
                                                    managed services to improve your current IoT solution and
                                                    services even further.

                                                </div>
                                                <div class="more-arrow-link">
                                                    <a href="#">
                                                        Learn more <i class="far fa-long-arrow-right"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- ht-box-icon End -->
                                </div>


                                <div class="col-lg-4 col-md-4 image-box-boder">
                                    <!-- ht-box-icon Start -->
                                    <div class="ht-box-images style-09">
                                        <div class="image-box-wrap">
                                            <div class="box-image">
                                                <img src="<?php bloginfo('stylesheet_directory')?>/assets/images/services/devops.svg" class="img-fluid"
                                                     alt="">
                                            </div>
                                            <div class="content">
                                                <h6 class="heading">Infrastrutre & Devops</h6>
                                                <div class="text">DevOps will automate and optimize your IT
                                                    processes where our expert DevOps engineers will help attune the
                                                    delivery, deployment, development, security, and support of any
                                                    high-load, fail-safe system with microservices architecture — so
                                                    your business strategy can always rely on high-quality software.
                                                </div>
                                                <div class="more-arrow-link">
                                                    <a href="#">
                                                        Learn more <i class="far fa-long-arrow-right"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- ht-box-icon End -->
                                </div>

                                <div class="col-lg-4 col-md-4 image-box-boder">
                                    <!-- ht-box-icon Start -->
                                    <div class="ht-box-images style-09">
                                        <div class="image-box-wrap">
                                            <div class="box-image">
                                                <img src="<?php bloginfo('stylesheet_directory')?>/assets/images/services/uiux.svg" class="img-fluid" alt="">
                                            </div>
                                            <div class="content">
                                                <h6 class="heading">UI/UX Design</h6>
                                                <div class="text">Want to build your product with a team that
                                                    establishes a clear design process, meets deadlines, and
                                                    delivers a spot-on end result? Our design team has created a
                                                    large variety of applications and products for all major
                                                    industries. Our designers work with you to understand your
                                                    brand, challenges, business goals, and how your end users will
                                                    interact with your product or application.
                                                </div>
                                                <div class="more-arrow-link">
                                                    <a href="#">
                                                        Learn more <i class="far fa-long-arrow-right"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- ht-box-icon End -->
                                </div>

                                <div class="col-lg-4 col-md-4 image-box-boder">
                                    <!-- ht-box-icon Start -->
                                    <div class="ht-box-images style-09">
                                        <div class="image-box-wrap">
                                            <div class="box-image">
                                                <img src="<?php bloginfo('stylesheet_directory')?>/assets/images/services/marketing.svg" class="img-fluid"
                                                     alt="">
                                            </div>
                                            <div class="content">
                                                <h6 class="heading">Digital Marketing</h6>
                                                <div class="text">Our technology experts work hand in hand with your
                                                    marketing team to help them get the most out of the software and
                                                    platforms that underlie your digital marketing strategy in
                                                    return that promotes your brand and drives sales. We provide the
                                                    custom digital marketing solutions you need to add content
                                                    across multiple channels, integrate your existing systems, and
                                                    get your site running perfectly for great user experience.

                                                </div>
                                                <div class="more-arrow-link">
                                                    <a href="#">
                                                        Learn more <i class="far fa-long-arrow-right"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- ht-box-icon End -->
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="talk-message-box-wrapper section-space--mt_80 small-mt__60 d-none">
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="talk-message-box ">
                                <div class="message-icon">
                                    <i class="far fa-comment-alt-dots"></i>
                                </div>
                                <div class="talk-message-box-content ">
                                    <h6 class="heading">Cheers to the work that comes from trusted service providers
                                        in time.</h6>
                                    <a href="contact-us.php" class="ht-btn ht-btn-sm">Let's talk</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <!--===========  Feature Images Wrapper End =============-->


        <div class="tabs-wrapper  section-space--ptb_100">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-title-wrapper text-center section-space--mb_60 wow move-up">
                            <h3 class="section-title">Technologies we work with </h3>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 ht-tab-wrap">
                        <div class="row">
                            <div class="col-12 text-center wow move-up ">
                                <ul class="nav justify-content-center ht-tab-menu" role="tablist">
                                    <li class="tab__item nav-item active">
                                        <a class="nav-link active" id="nav-Mobile" data-toggle="tab"
                                           href="#Mobile-tab" role="tab" aria-selected="true">Mobile</a>
                                    </li>
                                    <li class="tab__item nav-item">
                                        <a class="nav-link" id="nav-FrontEnd" data-toggle="tab" href="#FrontEnd-tab"
                                           role="tab" aria-selected="false">Front End</a>
                                    </li>
                                    <li class="tab__item nav-item ">
                                        <a class="nav-link" id="nav-Database" data-toggle="tab" href="#Database-tab"
                                           role="tab" aria-selected="false">Database</a>
                                    </li>
                                    <li class="tab__item nav-item ">
                                        <a class="nav-link" id="nav-Backend" data-toggle="tab" href="#Backend-tab"
                                           role="tab" aria-selected="false">Backend</a>
                                    </li>
                                    <li class="tab__item nav-item ">
                                        <a class="nav-link" id="nav-CMS" data-toggle="tab" href="#CMS-tab"
                                           role="tab" aria-selected="false">CMS</a>
                                    </li>

                                    <li class="tab__item nav-item ">
                                        <a class="nav-link" id="nav-DevOps" data-toggle="tab" href="#DevOps-tab"
                                           role="tab" aria-selected="false">DevOps</a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                        <div class="tab-content ht-tab__content wow move-up">

                            <div class="tab-pane fade show active" id="Mobile-tab" role="tabpanel">
                                <div class="tab-history-wrap section-space--mt_60">
                                    <div class="row">
                                        <!--<div class="resizer"></div>-->
                                        <div class="col-lg-3 col-6 col-md-6">
                                            <!-- Projects Wrap Start -->
                                            <a href="javascript:" class="projects-wrap style-2">
                                                <div class="projects-image-box">
                                                    <div class="projects-image">
                                                        <img class="img-fluid"
                                                             src="<?php bloginfo('stylesheet_directory')?>/assets/images/technologies/ios.svg" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <h6 class="heading">iOS</h6>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Projects Wrap End -->
                                        </div>

                                        <div class="col-lg-3 col-6 col-md-6">
                                            <!-- Projects Wrap Start -->
                                            <a href="javascript:" class="projects-wrap style-2">
                                                <div class="projects-image-box">
                                                    <div class="projects-image">
                                                        <img class="img-fluid"
                                                             src="<?php bloginfo('stylesheet_directory')?>/assets/images/technologies/android.svg" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <h6 class="heading">Android</h6>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Projects Wrap End -->
                                        </div>

                                        <div class="col-lg-3 col-6 col-md-6">
                                            <!-- Projects Wrap Start -->
                                            <a href="javascript:" class="projects-wrap style-2">
                                                <div class="projects-image-box">
                                                    <div class="projects-image">
                                                        <img class="img-fluid"
                                                             src="<?php bloginfo('stylesheet_directory')?>/assets/images/technologies/React.svg" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <h6 class="heading">React Native</h6>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Projects Wrap End -->
                                        </div>

                                        <div class="col-lg-3 col-6 col-md-6">
                                            <!-- Projects Wrap Start -->
                                            <a href="javascript:" class="projects-wrap style-2">
                                                <div class="projects-image-box">
                                                    <div class="projects-image">
                                                        <img class="img-fluid"
                                                             src="<?php bloginfo('stylesheet_directory')?>/assets/images/technologies/Flutter.svg" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <h6 class="heading">Flutter</h6>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Projects Wrap End -->
                                        </div>

                                        <div class="col-lg-3 col-6 col-md-6">
                                            <!-- Projects Wrap Start -->
                                            <a href="javascript:" class="projects-wrap style-2">
                                                <div class="projects-image-box">
                                                    <div class="projects-image">
                                                        <img class="img-fluid"
                                                             src="<?php bloginfo('stylesheet_directory')?>/assets/images/technologies/Ionic.svg" alt="">
                                                    </div>
                                                    <div class="content">
                                                        <h6 class="heading">Ionic</h6>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Projects Wrap End -->
                                        </div>

                                        <div class="col-lg-3 col-6 col-md-6">
                                            <!-- Projects Wrap Start -->
                                            <a href="javascript:" class="projects-wrap style-2">
                                                <div class="projects-image-box">
                                                    <div class="projects-image">
                                                        <img class="img-fluid"
                                                             src="<?php bloginfo('stylesheet_directory')?>/assets/images/technologies/Swift.svg" alt="">
                                                    </div>
                                                    <div class="content">

                                                        <h6 class="heading">Swift</h6>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Projects Wrap End -->
                                        </div>

                                        <div class="col-lg-3 col-6 col-md-6">
                                            <!-- Projects Wrap Start -->
                                            <a href="javascript:" class="projects-wrap style-2">
                                                <div class="projects-image-box">
                                                    <div class="projects-image">
                                                        <img class="img-fluid"
                                                             src="<?php bloginfo('stylesheet_directory')?>/assets/images/technologies/kotlin.svg" alt="">
                                                    </div>
                                                    <div class="content">

                                                        <h6 class="heading">Kotlin</h6>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Projects Wrap End -->
                                        </div>


                                        <div class="col-lg-3 col-6 col-md-6">
                                            <!-- Projects Wrap Start -->
                                            <a href="javascript:" class="projects-wrap style-2">
                                                <div class="projects-image-box">
                                                    <div class="projects-image">
                                                        <img class="img-fluid"
                                                             src="<?php bloginfo('stylesheet_directory')?>/assets/images/technologies/ObjectiveC.svg" alt="">
                                                    </div>
                                                    <div class="content">

                                                        <h6 class="heading">ObjectiveC</h6>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Projects Wrap End -->
                                        </div>

                                        <div class="col-lg-3 col-6 col-md-6">
                                            <!-- Projects Wrap Start -->
                                            <a href="javascript:" class="projects-wrap style-2">
                                                <div class="projects-image-box">
                                                    <div class="projects-image">
                                                        <img class="img-fluid"
                                                             src="<?php bloginfo('stylesheet_directory')?>/assets/images/technologies/Titanium.svg" alt="">
                                                    </div>
                                                    <div class="content">

                                                        <h6 class="heading">Titanium</h6>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Projects Wrap End -->
                                        </div>


                                        <div class="col-lg-3 col-6 col-md-6">
                                            <!-- Projects Wrap Start -->
                                            <a href="javascript:" class="projects-wrap style-2">
                                                <div class="projects-image-box">
                                                    <div class="projects-image">
                                                        <img class="img-fluid"
                                                             src="<?php bloginfo('stylesheet_directory')?>/assets/images/technologies/Xamarin.svg" alt="">
                                                    </div>
                                                    <div class="content">

                                                        <h6 class="heading">Xamarin</h6>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Projects Wrap End -->
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="FrontEnd-tab" role="tabpanel">
                                <div class="tab-history-wrap section-space--mt_60">
                                    <div class="row">

                                        <div class="col-lg-3 col-6 col-md-6">
                                            <!-- Projects Wrap Start -->
                                            <a href="javascript:" class="projects-wrap style-2">
                                                <div class="projects-image-box">
                                                    <div class="projects-image">
                                                        <img class="img-fluid"
                                                             src="<?php bloginfo('stylesheet_directory')?>/assets/images/technologies/Angular.svg" alt="">
                                                    </div>
                                                    <div class="content">

                                                        <h6 class="heading">Angular</h6>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Projects Wrap End -->
                                        </div>

                                        <div class="col-lg-3 col-6 col-md-6">
                                            <!-- Projects Wrap Start -->
                                            <a href="javascript:" class="projects-wrap style-2">
                                                <div class="projects-image-box">
                                                    <div class="projects-image">
                                                        <img class="img-fluid"
                                                             src="<?php bloginfo('stylesheet_directory')?>/assets/images/technologies/React Js.svg" alt="">
                                                    </div>
                                                    <div class="content">

                                                        <h6 class="heading">React Js</h6>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Projects Wrap End -->
                                        </div>

                                        <div class="col-lg-3 col-6 col-md-6">
                                            <!-- Projects Wrap Start -->
                                            <a href="javascript:" class="projects-wrap style-2">
                                                <div class="projects-image-box">
                                                    <div class="projects-image">
                                                        <img class="img-fluid"
                                                             src="<?php bloginfo('stylesheet_directory')?>/assets/images/technologies/TypeScript.svg" alt="">
                                                    </div>
                                                    <div class="content">

                                                        <h6 class="heading">TypeScript</h6>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Projects Wrap End -->
                                        </div>

                                        <div class="col-lg-3 col-6 col-md-6">
                                            <!-- Projects Wrap Start -->
                                            <a href="javascript:" class="projects-wrap style-2">
                                                <div class="projects-image-box">
                                                    <div class="projects-image">
                                                        <img class="img-fluid"
                                                             src="<?php bloginfo('stylesheet_directory')?>/assets/images/technologies/Vue.svg" alt="">
                                                    </div>
                                                    <div class="content">

                                                        <h6 class="heading">Vue</h6>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Projects Wrap End -->
                                        </div>

                                        <div class="col-lg-3 col-6 col-md-6">
                                            <!-- Projects Wrap Start -->
                                            <a href="javascript:" class="projects-wrap style-2">
                                                <div class="projects-image-box">
                                                    <div class="projects-image">
                                                        <img class="img-fluid"
                                                             src="<?php bloginfo('stylesheet_directory')?>/assets/images/technologies/WPF.svg" alt="">
                                                    </div>
                                                    <div class="content">

                                                        <h6 class="heading">WPF</h6>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Projects Wrap End -->
                                        </div>

                                        <div class="col-lg-3 col-6 col-md-6">
                                            <!-- Projects Wrap Start -->
                                            <a href="javascript:" class="projects-wrap style-2">
                                                <div class="projects-image-box">
                                                    <div class="projects-image">
                                                        <img class="img-fluid"
                                                             src="<?php bloginfo('stylesheet_directory')?>/assets/images/technologies/HTML5.svg" alt="">
                                                    </div>
                                                    <div class="content">

                                                        <h6 class="heading">HTML5</h6>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Projects Wrap End -->
                                        </div>


                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="Database-tab" role="tabpanel">
                                <div class="tab-history-wrap section-space--mt_60">
                                    <div class="row">

                                        <div class="col-lg-3 col-6 col-md-6">
                                            <!-- Projects Wrap Start -->
                                            <a href="javascript:" class="projects-wrap style-2">
                                                <div class="projects-image-box">
                                                    <div class="projects-image">
                                                        <img class="img-fluid"
                                                             src="<?php bloginfo('stylesheet_directory')?>/assets/images/technologies/Mongo DB.svg" alt="">
                                                    </div>
                                                    <div class="content">

                                                        <h6 class="heading">Mongo DB</h6>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Projects Wrap End -->
                                        </div>

                                        <div class="col-lg-3 col-6 col-md-6">
                                            <!-- Projects Wrap Start -->
                                            <a href="javascript:" class="projects-wrap style-2">
                                                <div class="projects-image-box">
                                                    <div class="projects-image">
                                                        <img class="img-fluid"
                                                             src="<?php bloginfo('stylesheet_directory')?>/assets/images/technologies/MySQL.svg" alt="">
                                                    </div>
                                                    <div class="content">

                                                        <h6 class="heading">MySQL</h6>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Projects Wrap End -->
                                        </div>

                                        <div class="col-lg-3 col-6 col-md-6">
                                            <!-- Projects Wrap Start -->
                                            <a href="javascript:" class="projects-wrap style-2">
                                                <div class="projects-image-box">
                                                    <div class="projects-image">
                                                        <img class="img-fluid"
                                                             src="<?php bloginfo('stylesheet_directory')?>/assets/images/technologies/MsSQL.svg" alt="">
                                                    </div>
                                                    <div class="content">

                                                        <h6 class="heading">MsSQL</h6>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Projects Wrap End -->
                                        </div>

                                        <div class="col-lg-3 col-6 col-md-6">
                                            <!-- Projects Wrap Start -->
                                            <a href="javascript:" class="projects-wrap style-2">
                                                <div class="projects-image-box">
                                                    <div class="projects-image">
                                                        <img class="img-fluid"
                                                             src="<?php bloginfo('stylesheet_directory')?>/assets/images/technologies/Firebase.svg" alt="">
                                                    </div>
                                                    <div class="content">

                                                        <h6 class="heading">Firebase</h6>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Projects Wrap End -->
                                        </div>

                                        <div class="col-lg-3 col-6 col-md-6">
                                            <!-- Projects Wrap Start -->
                                            <a href="javascript:" class="projects-wrap style-2">
                                                <div class="projects-image-box">
                                                    <div class="projects-image">
                                                        <img class="img-fluid"
                                                             src="<?php bloginfo('stylesheet_directory')?>/assets/images/technologies/Realm.svg" alt="">
                                                    </div>
                                                    <div class="content">

                                                        <h6 class="heading">Realm</h6>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Projects Wrap End -->
                                        </div>

                                        <div class="col-lg-3 col-6 col-md-6">
                                            <!-- Projects Wrap Start -->
                                            <a href="javascript:" class="projects-wrap style-2">
                                                <div class="projects-image-box">
                                                    <div class="projects-image">
                                                        <img class="img-fluid"
                                                             src="<?php bloginfo('stylesheet_directory')?>/assets/images/technologies/DynamoDB.svg" alt="">
                                                    </div>
                                                    <div class="content">

                                                        <h6 class="heading">DynamoDB</h6>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Projects Wrap End -->
                                        </div>

                                        <div class="col-lg-3 col-6 col-md-6">
                                            <!-- Projects Wrap Start -->
                                            <a href="javascript:" class="projects-wrap style-2">
                                                <div class="projects-image-box">
                                                    <div class="projects-image">
                                                        <img class="img-fluid"
                                                             src="<?php bloginfo('stylesheet_directory')?>/assets/images/technologies/Oracle.svg" alt="">
                                                    </div>
                                                    <div class="content">

                                                        <h6 class="heading">Oracle</h6>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Projects Wrap End -->
                                        </div>

                                        <div class="col-lg-3 col-6 col-md-6">
                                            <!-- Projects Wrap Start -->
                                            <a href="javascript:" class="projects-wrap style-2">
                                                <div class="projects-image-box">
                                                    <div class="projects-image">
                                                        <img class="img-fluid"
                                                             src="<?php bloginfo('stylesheet_directory')?>/assets/images/technologies/PostgreSQL.svg" alt="">
                                                    </div>
                                                    <div class="content">

                                                        <h6 class="heading">PostgreSQL</h6>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Projects Wrap End -->
                                        </div>

                                        <div class="col-lg-3 col-6 col-md-6 d-none">
                                            <!-- Projects Wrap Start -->
                                            <a href="javascript:" class="projects-wrap style-2">
                                                <div class="projects-image-box">
                                                    <div class="projects-image">
                                                        <img class="img-fluid"
                                                             src="<?php bloginfo('stylesheet_directory')?>/assets/images/technologies/Redis.svg" alt="">
                                                    </div>
                                                    <div class="content">

                                                        <h6 class="heading">Redis</h6>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Projects Wrap End -->
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="Backend-tab" role="tabpanel">
                                <div class="tab-history-wrap section-space--mt_60">
                                    <div class="row">

                                        <div class="col-lg-3 col-6 col-md-6">
                                            <!-- Projects Wrap Start -->
                                            <a href="javascript:" class="projects-wrap style-2">
                                                <div class="projects-image-box">
                                                    <div class="projects-image">
                                                        <img class="img-fluid"
                                                             src="<?php bloginfo('stylesheet_directory')?>/assets/images/technologies/PHP.svg" alt="">
                                                    </div>
                                                    <div class="content">

                                                        <h6 class="heading">PHP</h6>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Projects Wrap End -->
                                        </div>

                                        <div class="col-lg-3 col-6 col-md-6">
                                            <!-- Projects Wrap Start -->
                                            <a href="javascript:" class="projects-wrap style-2">
                                                <div class="projects-image-box">
                                                    <div class="projects-image">
                                                        <img class="img-fluid"
                                                             src="<?php bloginfo('stylesheet_directory')?>/assets/images/technologies/Java.svg" alt="">
                                                    </div>
                                                    <div class="content">

                                                        <h6 class="heading">Java</h6>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Projects Wrap End -->
                                        </div>


                                        <div class="col-lg-3 col-6 col-md-6">
                                            <!-- Projects Wrap Start -->
                                            <a href="javascript:" class="projects-wrap style-2">
                                                <div class="projects-image-box">
                                                    <div class="projects-image">
                                                        <img class="img-fluid"
                                                             src="<?php bloginfo('stylesheet_directory')?>/assets/images/technologies/NET.svg" alt="">
                                                    </div>
                                                    <div class="content">

                                                        <h6 class="heading">.NET</h6>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Projects Wrap End -->
                                        </div>


                                        <div class="col-lg-3 col-6 col-md-6">
                                            <!-- Projects Wrap Start -->
                                            <a href="javascript:" class="projects-wrap style-2">
                                                <div class="projects-image-box">
                                                    <div class="projects-image">
                                                        <img class="img-fluid"
                                                             src="<?php bloginfo('stylesheet_directory')?>/assets/images/technologies/Node.svg" alt="">
                                                    </div>
                                                    <div class="content">

                                                        <h6 class="heading">Node .Js</h6>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Projects Wrap End -->
                                        </div>


                                        <div class="col-lg-3 col-6 col-md-6">
                                            <!-- Projects Wrap Start -->
                                            <a href="javascript:" class="projects-wrap style-2">
                                                <div class="projects-image-box">
                                                    <div class="projects-image">
                                                        <img class="img-fluid"
                                                             src="<?php bloginfo('stylesheet_directory')?>/assets/images/technologies/Rails.svg" alt="">
                                                    </div>
                                                    <div class="content">

                                                        <h6 class="heading">Rails</h6>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Projects Wrap End -->
                                        </div>

                                        <div class="col-lg-3 col-6 col-md-6">
                                            <!-- Projects Wrap Start -->
                                            <a href="javascript:" class="projects-wrap style-2">
                                                <div class="projects-image-box">
                                                    <div class="projects-image">
                                                        <img class="img-fluid"
                                                             src="<?php bloginfo('stylesheet_directory')?>/assets/images/technologies/Python.svg" alt="">
                                                    </div>
                                                    <div class="content">

                                                        <h6 class="heading">Python</h6>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Projects Wrap End -->
                                        </div>

                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="CMS-tab" role="tabpanel">
                                <div class="tab-history-wrap section-space--mt_60">
                                    <div class="row">

                                        <div class="col-lg-3 col-6 col-md-6">
                                            <!-- Projects Wrap Start -->
                                            <a href="javascript:" class="projects-wrap style-2">
                                                <div class="projects-image-box">
                                                    <div class="projects-image">
                                                        <img class="img-fluid"
                                                             src="<?php bloginfo('stylesheet_directory')?>/assets/images/technologies/Drupal.svg" alt="">
                                                    </div>
                                                    <div class="content">

                                                        <h6 class="heading">Drupal</h6>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Projects Wrap End -->
                                        </div>

                                        <div class="col-lg-3 col-6 col-md-6">
                                            <!-- Projects Wrap Start -->
                                            <a href="javascript:" class="projects-wrap style-2">
                                                <div class="projects-image-box">
                                                    <div class="projects-image">
                                                        <img class="img-fluid"
                                                             src="<?php bloginfo('stylesheet_directory')?>/assets/images/technologies/Joomla.svg" alt="">
                                                    </div>
                                                    <div class="content">

                                                        <h6 class="heading">Joomla</h6>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Projects Wrap End -->
                                        </div>

                                        <div class="col-lg-3 col-6 col-md-6">
                                            <!-- Projects Wrap Start -->
                                            <a href="javascript:" class="projects-wrap style-2">
                                                <div class="projects-image-box">
                                                    <div class="projects-image">
                                                        <img class="img-fluid"
                                                             src="<?php bloginfo('stylesheet_directory')?>/assets/images/technologies/Wordpress.svg" alt="">
                                                    </div>
                                                    <div class="content">

                                                        <h6 class="heading">Wordpress</h6>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Projects Wrap End -->
                                        </div>


                                        <div class="col-lg-3 col-6 col-md-6">
                                            <!-- Projects Wrap Start -->
                                            <a href="javascript:" class="projects-wrap style-2">
                                                <div class="projects-image-box">
                                                    <div class="projects-image">
                                                        <img class="img-fluid"
                                                             src="<?php bloginfo('stylesheet_directory')?>/assets/images/technologies/Magento.svg" alt="">
                                                    </div>
                                                    <div class="content">

                                                        <h6 class="heading">Magento</h6>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Projects Wrap End -->
                                        </div>


                                        <div class="col-lg-3 col-6 col-md-6">
                                            <!-- Projects Wrap Start -->
                                            <a href="javascript:" class="projects-wrap style-2">
                                                <div class="projects-image-box">
                                                    <div class="projects-image">
                                                        <img class="img-fluid"
                                                             src="<?php bloginfo('stylesheet_directory')?>/assets/images/technologies/Shopify.svg" alt="">
                                                    </div>
                                                    <div class="content">

                                                        <h6 class="heading">Shopify</h6>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Projects Wrap End -->
                                        </div>


                                    </div>
                                </div>
                            </div>

                            <div class="tab-pane fade" id="DevOps-tab" role="tabpanel">
                                <div class="tab-history-wrap section-space--mt_60">
                                    <div class="row">

                                        <div class="col-lg-3 col-6 col-md-6">
                                            <!-- Projects Wrap Start -->
                                            <a href="javascript:" class="projects-wrap style-2">
                                                <div class="projects-image-box">
                                                    <div class="projects-image">
                                                        <img class="img-fluid"
                                                             src="<?php bloginfo('stylesheet_directory')?>/assets/images/technologies/AWS.svg" alt="">
                                                    </div>
                                                    <div class="content">

                                                        <h6 class="heading">AWS</h6>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Projects Wrap End -->
                                        </div>

                                        <div class="col-lg-3 col-6 col-md-6">
                                            <!-- Projects Wrap Start -->
                                            <a href="javascript:" class="projects-wrap style-2">
                                                <div class="projects-image-box">
                                                    <div class="projects-image">
                                                        <img class="img-fluid"
                                                             src="<?php bloginfo('stylesheet_directory')?>/assets/images/technologies/Google Cloud.svg"
                                                             alt="">
                                                    </div>
                                                    <div class="content">

                                                        <h6 class="heading">Google Cloud</h6>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Projects Wrap End -->
                                        </div>


                                        <div class="col-lg-3 col-6 col-md-6">
                                            <!-- Projects Wrap Start -->
                                            <a href="javascript:" class="projects-wrap style-2">
                                                <div class="projects-image-box">
                                                    <div class="projects-image">
                                                        <img class="img-fluid"
                                                             src="<?php bloginfo('stylesheet_directory')?>/assets/images/technologies/Gradle.svg" alt="">
                                                    </div>
                                                    <div class="content">

                                                        <h6 class="heading">Gradle</h6>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Projects Wrap End -->
                                        </div>


                                        <div class="col-lg-3 col-6 col-md-6">
                                            <!-- Projects Wrap Start -->
                                            <a href="javascript:" class="projects-wrap style-2">
                                                <div class="projects-image-box">
                                                    <div class="projects-image">
                                                        <img class="img-fluid"
                                                             src="<?php bloginfo('stylesheet_directory')?>/assets/images/technologies/Jenkins.svg" alt="">
                                                    </div>
                                                    <div class="content">

                                                        <h6 class="heading">Jenkins</h6>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Projects Wrap End -->
                                        </div>


                                        <div class="col-lg-3 col-6 col-md-6">
                                            <!-- Projects Wrap Start -->
                                            <a href="javascript:" class="projects-wrap style-2">
                                                <div class="projects-image-box">
                                                    <div class="projects-image">
                                                        <img class="img-fluid"
                                                             src="<?php bloginfo('stylesheet_directory')?>/assets/images/technologies/Appium.svg" alt="">
                                                    </div>
                                                    <div class="content">

                                                        <h6 class="heading">Appium</h6>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Projects Wrap End -->
                                        </div>


                                        <div class="col-lg-3 col-6 col-md-6">
                                            <!-- Projects Wrap Start -->
                                            <a href="javascript:" class="projects-wrap style-2">
                                                <div class="projects-image-box">
                                                    <div class="projects-image">
                                                        <img class="img-fluid"
                                                             src="<?php bloginfo('stylesheet_directory')?>/assets/images/technologies/Seleinum.svg" alt="">
                                                    </div>
                                                    <div class="content">

                                                        <h6 class="heading">Seleinum</h6>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Projects Wrap End -->
                                        </div>

                                        <div class="col-lg-3 col-6 col-md-6">
                                            <!-- Projects Wrap Start -->
                                            <a href="javascript:" class="projects-wrap style-2">
                                                <div class="projects-image-box">
                                                    <div class="projects-image">
                                                        <img class="img-fluid"
                                                             src="<?php bloginfo('stylesheet_directory')?>/assets/images/technologies/azure.svg" alt="">
                                                    </div>
                                                    <div class="content">

                                                        <h6 class="heading">Azure</h6>
                                                    </div>
                                                </div>
                                            </a>
                                            <!-- Projects Wrap End -->
                                        </div>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <!--====================  Conact us Section Start ====================-->
        <div class="contact-us-section-wrappaer processing-contact-us-bg section-space--ptb_120">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-lg-6 col-lg-6">
                        <div class="conact-us-wrap-one">
                            <h3 class="heading text-white">To get further information please contact us</h3>

                            <div class="sub-heading text-white">We’re available for 24/7</div>

                        </div>
                    </div>

                    <div class="col-lg-6 col-lg-6">
                        <div class="contact-info-two text-center">
                            <div class="icon">
                                <span class="fal fa-phone"></span>
                            </div>
                            <h6 class="heading font-weight--reguler">Reach out now!</h6>
                            <h2 class="call-us"><a href="tel:+9221111087423">+9221111087423</a></h2>
                            <div class="contact-us-button mt-20">
                                <a href="/contact-us" class="btn btn--secondary">Contact us</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--====================  Conact us Section End  ====================-->

        <!--==================== Claim To Excel Area Start ====================-->
        <div class="claim-to-excel-area section-space--ptb_120">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <!-- section-title-wrap Start -->
                        <div class="section-title-wrap text-center section-space--mb_60">
                            <h2 class="heading">5-Step Customer Relationship Roadmap</h2>
                        </div>
                        <!-- section-title-wrap Start -->
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-12">
                        <div class="claim-grid-group">
                            <div class="ht-box-icon style-04">
                                <div class="icon-box-wrap color-one">
                                    <div class="icon">
                                        <span class="icon-basic-anticlockwise"></span>
                                    </div>
                                    <div class="content">
                                        <h6 class="sub-heading">01</h6>
                                        <h5 class="heading">Problem Understanding</h5>
                                        <p class="text">is critical to establish a strong foundation to work
                                            together</p>
                                    </div>
                                </div>
                            </div>
                            <div class="ht-box-icon style-04">
                                <div class="icon-box-wrap color-two">
                                    <div class="icon">
                                        <span class="icon-basic-life-buoy"></span>
                                    </div>
                                    <div class="content">
                                        <h6 class="sub-heading">02</h6>
                                        <h5 class="heading">Optimized Digital Solutions</h5>
                                        <p class="text">involves creative and goal oriented thought processes</p>
                                    </div>
                                </div>
                            </div>
                            <div class="ht-box-icon style-04">
                                <div class="icon-box-wrap color-three">
                                    <div class="icon">
                                        <span class="icon-basic-world"></span>
                                    </div>
                                    <div class="content">
                                        <h6 class="sub-heading">03</h6>
                                        <h5 class="heading">Value for Money</h5>
                                        <p class="text"> is measured through quick turnaround with uncompromised
                                            quality</p>
                                    </div>
                                </div>
                            </div>
                            <div class="ht-box-icon style-04">
                                <div class="icon-box-wrap color-four">
                                    <div class="icon">
                                        <span class="icon-basic-case"></span>
                                    </div>
                                    <div class="content">
                                        <h6 class="sub-heading">04</h6>
                                        <h5 class="heading">Trusted Relationship</h5>
                                        <p class="text"> is achieved only through transparency & predictability of
                                            services</p>
                                    </div>
                                </div>
                            </div>
                            <div class="ht-box-icon style-04">
                                <div class="icon-box-wrap color-five">
                                    <div class="icon">
                                        <span class="icon-basic-lock"></span>
                                    </div>
                                    <div class="content">
                                        <h6 class="sub-heading">05</h6>
                                        <h5 class="heading"> Strategic Partnership</h5>
                                        <p class="text"> is a natural outcome of a mutually beneficial service
                                            model </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
        </div>
        <!--==================== Claim To Excel Area End ====================-->


    </div>



<?php get_footer();?>