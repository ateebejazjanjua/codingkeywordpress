<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'cdw_codingkey' );

/** MySQL database username */
define( 'DB_USER', 'cdw_codingkey' );

/** MySQL database password */
define( 'DB_PASSWORD', 'kSCoEzcXcBFLO2GN' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

define('FORCE_SSL_ADMIN', true);
define('FORCE_SSL_LOGIN', true);

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'x$B9pkjNf#QPStxs}tBbQshRBh<89YBg)^K(UNClMay 5ic&Us2.E/okM*G3w/iR' );
define( 'SECURE_AUTH_KEY',  'iGuX[Q[1jur1[&;7XBORJ(,Wm]=)Wxe5B@T*HO0E{:<u&Yr;eO!|]ufaEd_:Gxc1' );
define( 'LOGGED_IN_KEY',    'O~Y:Yo}T`;kf]P6nUr(, zW8#UW#)a$gSpyWmFDa[V9p#B8Sw|DLKx|QpkAt}$mj' );
define( 'NONCE_KEY',        '*{<szJUwD?isL25*IEi |4qpZ{QF^$yZT@$2fxxw|r!G|gZ4M]T|(8J6@]j5;wGm' );
define( 'AUTH_SALT',        'a+<ehfZ0E7X7xWn$F=c@#eCcY|Yc9pN9J,,<#pZL/AFhp2gq0yiRGPK88 RB:-Mb' );
define( 'SECURE_AUTH_SALT', '#yRUQskitrL>I`H827~l*t?|JfvQ]p._x$)V(o&xQFX5rkJZ5.oW133>+VdKq<oC' );
define( 'LOGGED_IN_SALT',   '1iqIbStJ<Bg] :lT]ee<RDZnb^>La/ 6>=T7$oV8IORgb8t({!7p&1i)QK>M4c0^' );
define( 'NONCE_SALT',       '<-t4Ne<z3Qo4H2,@P+~uBZ)Jr;}R8[vn7O1>{A@l/#1oWZ,H,VHwHltg[<pkCVsT' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'ck_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

define( 'AUTOSAVE_INTERVAL', 300 );
define( 'WP_POST_REVISIONS', 5 );
define( 'EMPTY_TRASH_DAYS', 7 );
define( 'WP_CRON_LOCK_TIMEOUT', 120 );
/* Add any custom values between this line and the "stop editing" line. */



//define( 'AUTOSAVE_INTERVAL', 300 );
//define( 'WP_POST_REVISIONS', 5 );
//define( 'EMPTY_TRASH_DAYS', 7 );
//define( 'WP_CRON_LOCK_TIMEOUT', 120 );
/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
